const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
// Retorna todos os produtos
router.get('/', (req, res, next) => {
    res.status(200).send({
        mensagem: 'Retorna todos os produto'
    });
});
// Insere um produto
router.post('/', (req, res, next) => {
    const produto = {
        nome: req.body.nome,
        preco: req.body.preco
    };

    return res.status(201).send({ 
        mensagem: 'Insere um produto',
        produtoCriado: produto 
    });
});

router.get('/:id_produto', (req, res, next) => {
    const id = req.params.id_produto

    if (id === 'especial') {
        res.status(200).send({
            mensagem: 'Você descobriu o ID especial',
            id: id
        });
    } else {
        res.status(200).send({
            mensagem: 'Você passou um ID',
        });
    }


});

router.patch('/', (req, res, next) => {

    res.status(201).send({
        mensagem: 'Produto alterado'
    });
});

router.delete('/', (req, res, next) => {

    res.status(201).send({
        mensagem: 'Produto excluido'
    });
});

module.exports = router;